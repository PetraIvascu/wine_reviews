"""module utils.py contains methods which help for data preprocessing, trianing
"""

import pickle
import pandas as pd
import numpy as np
import joblib


def save_pipeline(pipeline, location):
  """save pipeline

  Arguments:
      pipeline {sklearn pipelin} -- pipeline of transformations
      location {str} -- where to save includes additional name
  """
  joblib.dump(pipeline, location)


def load_pipeline(location):
  """loads pipeline

  Arguments:
      location {str} -- where to load it from includes additional name

  Returns:
      {sklearn pipelin} -- pipeline of transformations
  """
  pipeline = joblib.load(location)
  return pipeline


def save_tokenizer(tokenizer, location_save):
  """saves tokenizer

  Arguments:
      tokenizer {keras tokenizer} -- object to convert text to vectors
      location_save {str} -- where to save includes additional name
  """
  with open(location_save, 'wb') as handle:
    pickle.dump(tokenizer, handle, protocol=pickle.HIGHEST_PROTOCOL)


def load(location_load):
  """loads tokenizer

  Arguments:
      location_load {str} -- where to load it from includes additional name

  Returns:
       -- loaded pickle object
  """
  with open(location_load, 'rb') as handle:
    loaded_obj = pickle.load(handle)
  return loaded_obj


def load_embeddings_glove(location_embeddings):
  """loads glove embeddings

    Keyword Arguments:
        loaction_embeddings {str} -- path to embeddings

    Returns:
        [dict] -- words as keys and
                  corresponding dimensional vectors as values
    """

  embeddings_dictionary = dict()
  glove_file = open(location_embeddings, encoding="utf8")

  for line in glove_file:
    records = line.split()
    word = records[0]
    vector_dimensions = np.asarray(records[1:], dtype='float32')
    embeddings_dictionary[word] = vector_dimensions
  glove_file.close()

  return embeddings_dictionary


def split_data(data, train_size=0.8, validation_size=0.2):
  """splits pandas into training part and validation part

    Keyword Arguments:
        data {dataframe} -- pandas with all data processed
        train_size {float} -- how much to sample from training (default: {0.8})
        validation_size {float} -- how much to sample from validation (default: {0.2})
    """
  train = data.sample(frac=train_size)
  validation = data.sample(frac=validation_size)

  return train, validation


def summary(df):
  """ produces a summary for given data

    Arguments:
        df {data frame} -- data from csv file

    Returns:
        [data frame] -- contains the summary
    """
  dict_summary = {}
  dict_summary.setdefault("name", [])
  dict_summary.setdefault("type", [])
  dict_summary.setdefault("unique_values", [])
  dict_summary.setdefault("missing_values", [])
  dict_summary.setdefault("mean", [])
  dict_summary.setdefault("std", [])
  dict_summary.setdefault("min", [])
  dict_summary.setdefault("max", [])
  dict_summary.setdefault("outliers", [])
  dict_summary.setdefault("outliers_percentual", [])

  described_data = df.describe()

  for col in df.columns.to_list()[1:]:  # excludes id column
    dict_summary["name"].append(col)
    dict_summary["type"].append(str(df[col].dtype))
    dict_summary["unique_values"].append(df[col].nunique())
    dict_summary["missing_values"].append(df[col].isnull().sum())

    if 'features' in col:
      dict_summary["min"].append(len(df[col].min()))
      dict_summary["max"].append(len(df[col].max()))
      dict_summary["mean"].append(np.NaN)
      dict_summary["std"].append(np.NaN)
      dict_summary["outliers"].append(np.NaN)
      dict_summary["outliers_percentual"].append(np.NaN)

    if 'continous' in col:
      dict_summary["mean"].append(described_data[col]["mean"])
      dict_summary["std"].append(described_data[col]["std"])
      dict_summary["min"].append(described_data[col]["min"])
      dict_summary["max"].append(described_data[col]["max"])
      outliers_cut = described_data[col]["std"] * 2.5
      bound_inf = described_data[col]["mean"] - outliers_cut
      bound_sup = described_data[col]["mean"] + outliers_cut
      outliers = [x for x in df[col] if x < bound_inf or x > bound_sup]
      non_outliers = [x for x in df[col] if x > bound_inf and x < bound_sup]
      dict_summary["outliers"].append(len(outliers))
      dict_summary["outliers_percentual"].append(
          round((len(outliers) / len(non_outliers)) * 100, 4))

    if 'categorical' in col:
      dict_summary["mean"].append(np.NaN)
      dict_summary["std"].append(np.NaN)
      dict_summary["min"].append(np.NaN)
      dict_summary["max"].append(np.NaN)
      dict_summary["outliers"].append(np.NaN)
      dict_summary["outliers_percentual"].append(np.NaN)

  return pd.DataFrame(dict_summary)
