#!/usr/bin/env python
"""summary.py sumarizes given data
"""
import sys
import pandas as pd
from utils import summary


def main(argv):
  """creates summary based on the data read from given path
     from command line
     saves results to csv in dir. data
  Arguments:
      argv {str} -- path to data eg.'./data/raw/train/train_data.csv'
  """
  path_to_data = argv[1]
  data = pd.read_csv(path_to_data)
  data.rename(columns={'Unnamed: 0': 'id'}, inplace=True)
  data.rename(
      columns={'continuous_target_1': 'continous_target_1'}, inplace=True)
  summ = summary(data)
  summ.to_csv('./data/summary.csv', index=False)


if __name__ == "__main__":
  main(sys.argv)