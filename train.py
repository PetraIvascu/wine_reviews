#!/usr/bin/env python

from entities.wine_train import WineModelTrain


def main():
  wine_train = WineModelTrain()
  # reads data from location defined in eval.py config file
  wine_train.read_data()
  # processes data: inputs for missing values, converts data, scales data
  wine_train.transform_data()
  # creates model and coompiles it
  wine_train.build_model()
  # fits model using parameters given in configs/model.py
  # shows final result on validation
  wine_train.fit_model()
  # saves trained model
  wine_train.save_model()
  # evaluates validation set
  metrics = wine_train.evaluate_model()
  print('loss validation: %.2f mae validation: %.2f' % (metrics[0], metrics[1]))


if __name__ == "__main__":
  main()