#!/usr/bin/env python
"""feature_extractor.py extracts features using various pre-trained models
"""
import os
import pickle as pkl
import numpy as np
import pandas as pd
import tensorflow as tf
import tensorflow_hub as hub

from configs import configs


class Elmo:
  """extracts embedding vectors from elmo model
  """

  def __init__(self):
    self.__sess = tf.Session()
    self.__config = configs["feature_extraction"]
    self.__elmo = hub.Module(self.__config["location_elmo"], trainable=True)

  def __call__(self, ids, text):
    self.__ids = ids
    self.__text = text

  def _get_ebeddings(self, text):
    """creates embedding vectors for text

    Arguments:
        text {ndarray} -- text features

    Returns:
        [ndarray float32] -- embedding vectors elmo
        (see doc: https://tfhub.dev/google/elmo/3)
    """
    embeddings = self.__elmo(
        text.tolist(), signature="default", as_dict=True)["elmo"]
    self.__sess.run(tf.global_variables_initializer())
    self.__sess.run(tf.tables_initializer())
    embeddings = self.__sess.run(tf.reduce_mean(embeddings, 1))

    return embeddings

  def _batch_text(self):
    """creates shards of data for easy computing with elmo

    Returns:
        list of lists -- list grouping together every 100
    """
    batched_text = [
        self.__text[i:i + 100] for i in range(0, self.__text.shape[0], 100)
    ]
    return batched_text

  def _pairs_id_embedding(self, embeddings):
    """asociates id with elmo vector

    Arguments:
        embeddings {ndarray} -- float32 vectors from elmo

    Returns:
        dict -- key is the original id, value embedding vector elmo
    """
    dict_id_emb = {}
    for i, emb in enumerate(embeddings):
      dict_id_emb[self.__ids[i]] = emb
    return dict_id_emb

  def _save_embeddings(self, embeddings):
    """saving embedding vectors

    Arguments:
        embeddings {ndarray} -- [batch_size, 1024], vectors from elmo
    """
    ids_emb = self._pairs_id_embedding(embeddings)
    pickle_out = open(self.__config["embeddings_elmo"], "wb")
    pkl.dump(ids_emb, pickle_out)
    pickle_out.close()

  def extract_features(self):
    """converts text to embeddings using elmo and saves them
    """
    batched_text = self._batch_text()
    embeddings = [self._get_ebeddings(batch) for batch in batched_text]
    embeddings_new = np.concatenate(embeddings, axis=0)
    self._save_embeddings(embeddings_new)


if __name__ == "__main__":
  data_location = os.path.join(os.getcwd(), "data", "raw", "train",
                               "train_data.csv")
  # load data
  data = pd.read_csv(data_location)
  data.rename(columns={'Unnamed: 0': 'id'}, inplace=True)
  #data = data.sample(frac=0.001)

  # extracting features using elmo
  elmo = Elmo()
  elmo(data.id.to_numpy(), data.features.to_numpy())
  elmo.extract_features()