"""
model.py config file containig hyperparameters for class WineModel

"""

from __future__ import absolute_import
import os

training_params = {
    "loss":
    "mean_squared_error",
    "lr":
    0.001,
    "times_repeat_evaluation":
    20,
    "location_model":
    os.path.join(os.getcwd(), "model", "wine_model_dropout_test_true.h5"),
    "location_pipeline_numerical":
    os.path.join(os.getcwd(), "model", "pipeline_numerical.joblib"),
    "save_fig":
    os.path.join(os.getcwd(), "figures")
}
