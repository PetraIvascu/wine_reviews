"""
feature_extraction.py config file containig flags for feature extraction done on
text
"""

from __future__ import absolute_import
import os

training_params = {
    "location_elmo":
    "https://tfhub.dev/google/elmo/3",
    #os.path.join(os.getcwd(), "models_pretrained", "3.elmo"),
    "embeddings_elmo":
    os.path.join(os.getcwd(), "embeddings", "elmo_embeddings.pickle")
}
