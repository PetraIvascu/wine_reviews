"""
training.py config file containig hyperparameters for training model for
class: WineModelTrain

"""

from __future__ import absolute_import
import os

training_params = {
    "training_size":
    0.8,
    "validation_size":
    0.2,
    "dropout_training":
    True,
    "batch_size":
    512,
    "epochs":
    30,
    "shuffle":
    True,
    "verbosity":
    1,
    "location_data":
    os.path.join(os.getcwd(), "data", "raw", "train"),
    "location_model":
    os.path.join(os.getcwd(), "model", "wine_model_dropout_test_true.h5"),
    "location_pipeline_numerical":
    os.path.join(os.getcwd(), "model", "pipeline_numerical.joblib"),
}
