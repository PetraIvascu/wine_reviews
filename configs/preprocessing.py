"""
preprocessing.py config file containig flags for preprocessing data
class WinePreprocess
"""

from __future__ import absolute_import
import os

elmo = "3.elmo"
glove = "glove.6B.100d.txt"

training_params = {
    "glove_vectors":
    100,
    "location_pipeline_categorical":
    os.path.join(os.getcwd(), "model", "pipeline_categorical.joblib"),
    "location_tokenizer":
    os.path.join(os.getcwd(), "model", "tokenizer.pickle"),
    "location_embeddings":
    os.path.join(os.getcwd(), "embeddings", elmo)
}
