"""
eval.py config file containig hyperparameters for class WineModelEval

"""

from __future__ import absolute_import
import os

training_params = {
    "times_repeat_evaluation":
    50,
    "location_data":
    os.path.join(os.getcwd(), "data", "raw", "test"),
    "location_model":
    os.path.join(os.getcwd(), "model", "wine_model_dropout_test_true.h5"),
    "location_pipeline_numerical":
    os.path.join(os.getcwd(), "model", "pipeline_numerical.joblib"),
    "location_results":
    os.path.join(os.getcwd(), "model", "results_prediction.csv")
}
