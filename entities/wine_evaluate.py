"""wine_evaluate.py evaluate model the model
"""
import glob
import numpy as np
import pandas as pd

import utils
from entities.model import WineModel
from entities.preprocess import WinePreprocess
from configs import configs


class WineModelEval:
  """class evaluates wine model
  """

  def __init__(self):
    self.__data_processor = WinePreprocess(mode='production')
    self.__wine_model = WineModel()
    self.config = configs['eval']

  def read_data(self):
    """reads data from csv
    """
    files = glob.glob("{}/*{}".format(self.config['location_data'], '.csv'))
    data_location = files[0]

    data = pd.read_csv(data_location)
    data.rename(columns={'Unnamed: 0': 'id'}, inplace=True)
    data.rename(
        columns={'continuous_target_1': 'continous_target_1'}, inplace=True)

    self.__data = data
    self.__ids = data.id.to_numpy()

  def get_summary(self):
    """calls summary method from class resposible with preprocessing data

    Returns:
        [data frame] -- [description]
    """
    summary = self.__data_processor.summary(self.__data)
    return summary

  def transform_data(self):
    """ replaces NaN values and processes text, categorical values, etc.
    """
    self.__data = self.__data_processor.inputation(self.__data)
    self.copy_data = self.__data
    self.__data = self.__data_processor.transform(self.__data)
    # dropping id and features
    self.__data.drop(['id', 'features'], axis=1, inplace=True)
    self._scale_data()

  def _scale_data(self):
    """loads exsiting pipeline and scales data
    """
    pipeline_numerical = utils.load_pipeline(
        self.config['location_pipeline_numerical'])
    normalized_column, _ = self.__data_processor.scale_data(
        self.__data,
        pipeline_numerical,
        'Scaler',
        'continous_target_2',
        fit_transform=False)

    self.__data['continous_target_2'] = np.squeeze(normalized_column, axis=1)

  def load_model(self):
    """saves model
    """
    self.__wine_model.load()

  def evaluate_model(self):
    """evaluates model using x and y
    """
    metrics = self.__wine_model.eval(self.__data)
    return metrics

  def predict_uncertainty(self, top_k):
    """predicts uncertainty and saves results to csv

    Arguments:
        top_k {int} -- top k instances to be computed by evaluation report

    Returns:
        [tuple of data_frame] -- reports made on predictions
    """
    results = self.__wine_model.predict_cetainty(
        self.__ids, self.__data, self.config['times_repeat_evaluation'])
    df_results = self.__wine_model.result_uncertainty_to_data_frame(
        results, self.config['location_results'])
    top_k_cat_1, top_k_cat_2 = self.__wine_model.evaluation_report(
        df_results, self.copy_data, top_k)
    return (top_k_cat_1, top_k_cat_2)

  def display_report(self, report):
    """prints report

    Arguments:
        report {[data frame]} -- evaluation report
    """

    columns = report.columns.to_list()
    np_report = report.to_numpy()

    for obj in np_report:
      print(
          'For %s %s with %s %.2f ' % (columns[1], obj[1], columns[0], obj[0]))
    print('\n')
