"""wine_train.py trains the model
"""
import glob
import numpy as np
import pandas as pd
from sklearn import preprocessing
from sklearn import pipeline

import utils
from entities.model import WineModel
from entities.preprocess import WinePreprocess
from configs import configs


class WineModelTrain:
  """class trains wine model
  """

  def __init__(self):
    self.__data_processor = WinePreprocess(mode='train')
    self.__wine_model = WineModel()
    self.config = configs['training']

  def read_data(self):
    """reads data from csv
    """
    files = glob.glob("{}/*{}".format(self.config['location_data'], '.csv'))
    data_location = files[0]

    data = pd.read_csv(data_location)
    data.rename(columns={'Unnamed: 0': 'id'}, inplace=True)
    data.rename(
        columns={'continuous_target_1': 'continous_target_1'}, inplace=True)

    self.__data = data

  def transform_data(self):
    """ replaces NaN values and processes text, categorical values, etc.
        splits in train/val and scales data
    """
    self.__data = self.__data_processor.inputation(self.__data)
    self.__data = self.__data_processor.transform(self.__data)
    self.training, self.validation = self.__data_processor.split_data(
        self.__data, self.config['training_size'],
        self.config['validation_size'])
    self._scale_data(self.training, fit_transform=True)
    self._scale_data(self.validation, fit_transform=False)

  def _scale_data(self, data, fit_transform=True):
    """aggregates methods to scale data, values from scaling saved on disk
    for test time

    Arguments:
        data {data frame} -- data points x associated with their y

    Keyword Arguments:
        fit_transform {bool} -- needs to fit transformation (default: {True})
    """
    pipeline_numerical = pipeline.Pipeline([('Scaler',
                                             preprocessing.RobustScaler())])

    if fit_transform:
      normalized_column, pipeline_numerical = self.__data_processor.scale_data(
          data,
          pipeline_numerical,
          'Scaler',
          'continous_target_2',
          fit_transform=fit_transform)

      utils.save_pipeline(pipeline_numerical,
                          self.config['location_pipeline_numerical'])
    else:
      pipeline_numerical = utils.load_pipeline(
          self.config['location_pipeline_numerical'])
      normalized_column, _ = self.__data_processor.scale_data(
          data,
          pipeline_numerical,
          'Scaler',
          'continous_target_2',
          fit_transform=fit_transform)

    data['continous_target_2'] = np.squeeze(normalized_column, axis=1)

  def save_model(self):
    """saves model
    """
    self.__wine_model.save()

  def build_model(self):
    """ creates model and compiles it
    """
    numerical_features_size = self.__data.to_numpy()[:, 1:-2].shape[1]
    self.__wine_model.get_architecture(
        self.__data_processor.vocab_length,
        self.__data_processor.config['glove_vectors'],
        self.__data_processor.length_long_sentence,
        self.__data_processor.embedding_matrix, numerical_features_size,
        self.config['dropout_training'])

    self.__wine_model.complile()

    # save figure with architecture of model
    self.__wine_model.save_architecture()

  def fit_model(self):
    """trains model
    """
    self.__wine_model.fit(self.training, self.validation,
                          self.config['batch_size'], self.config['epochs'],
                          self.config['shuffle'], self.config['verbosity'])

  def evaluate_model(self):
    """evaluates model

    Returns:
        list of ndarrays -- metrics from evaluation
    """
    metrics = self.__wine_model.eval(self.validation)

    return metrics
