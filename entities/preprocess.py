"""preprocess.py module does all the preprocessings
"""
import numpy as np
import pandas as pd

from sklearn import pipeline
from sklearn import preprocessing
from keras.preprocessing.sequence import pad_sequences
from keras.preprocessing.text import Tokenizer
from nltk.tokenize import word_tokenize

import utils
from configs import configs


class WinePreprocess:
  """prepares data for training/prediction
  """

  def __init__(self, embeddings_type, mode=None):
    self.__mode = mode
    self.config = configs["preprocessing"]
    self.__embeddings = embeddings_type

  def inputation(self, data):
    """replaces NaN values
    Arguments:
      data {data frame} -- data to be transformed
    """

    nan_idx = data[pd.isnull(data.features)].index.tolist()
    if nan_idx:
      data.loc[nan_idx, 'features'] = ''

    data.continous_target_2 = data.fillna(data.continous_target_2.mean())

    data.categorical_target_1 = data.categorical_target_1.fillna('Unknown')

    country = data[
        data.categorical_target_2.isna()].categorical_target_1.to_numpy()
    if country.size:
      # mode within country
      entries_country = data.loc[data.categorical_target_1 == country[0]]
      data.categorical_target_2 = data.categorical_target_2.fillna(
          entries_country.categorical_target_2.value_counts().index[0])
    else:
      # mode on entire dataset
      mode_wine_type = data.categorical_target_2.value_counts().to_frame(
      ).index[0]
      data.categorical_target_2 = data.categorical_target_2.fillna(
          mode_wine_type)

    return data

  def _tokenization(self, text):
    """vectorizes a text

    Arguments:
        text {object} -- feature column to numpy
    """

    if self.__mode == 'train':
      word_tokenizer = Tokenizer(oov_token=157)
      word_tokenizer.fit_on_texts(text)
      utils.save_tokenizer(word_tokenizer, self.config['location_tokenizer'])

    if self.__mode == 'production':

      word_tokenizer = utils.load(self.config['location_tokenizer'])

    return word_tokenizer

  def text_to_vector_embeddings(self, text):
    """changes text to vector embeddings fit for training

    Arguments:
        text {(Python) objects} -- short descriptions
    """
    word_tokenizer = self._tokenization(text)
    self.vocab_length = len(word_tokenizer.word_index) + 1
    embedded_sentences = word_tokenizer.texts_to_sequences(text)

    # find number of words in longest sentence --> apply padding to the sentences
    word_count = lambda sentence: len(word_tokenize(sentence))
    longest_sentence = max(text, key=word_count)

    if self.__mode == 'train':
      self.length_long_sentence = len(word_tokenize(longest_sentence))
    if self.__mode == 'production':
      self.length_long_sentence = 156

    padded_sentences = pad_sequences(
        embedded_sentences, self.length_long_sentence, padding='post')

    # loads glove embeddings
    embeddings_dictionary = utils.load_embeddings_glove(
        self.config['location_embeddings'])

    # creates embedding matrix for our corpus
    self.embedding_matrix = np.zeros((self.vocab_length,
                                      self.config['glove_vectors']))
    for word, index in word_tokenizer.word_index.items():
      embedding_vector = embeddings_dictionary.get(word)
      if embedding_vector is not None:
        self.embedding_matrix[index] = embedding_vector

    return padded_sentences.tolist()

  def scale_data(self,
                 data,
                 pipeline_numerical,
                 transformation,
                 column,
                 fit_transform=True):
    """scales data by using: robust scaler - better with outliers

    Arguments:
        data {data frame} -- data to be transformed
        pipeline_numerical {Pipeline} -- encloses transformations which needed
                                         to be applied
        transformation {str} -- keyword to access appropriate transformation
        column {str} -- column to apply transformation on

    Returns:
        ndarray -- normalized column
        Pipeline -- Pipeline
    """
    if fit_transform:
      normalized_column = pipeline_numerical[transformation].fit(
          data[column].values.reshape(-1, 1))
    normalized_column = pipeline_numerical[transformation].transform(
        data[column].values.reshape(-1, 1))

    return normalized_column, pipeline_numerical

  def _categorical_to_ohe(self,
                          data,
                          pipeline_ohe,
                          transformation,
                          column,
                          fit=True):
    """converts categorical data to one hot encoded version

    Arguments:
        data {data frame} -- data to be transformed
        pipeline_ohe {Pipeline} -- pipeline eclosing transformations
        transformation {str} -- keyword to access appropriate transformation
        column {str} -- name of the column which needs the transformation to be
                        applied on

    Keyword Arguments:
        fit {bool} -- if transformation needs to be fit (default: {True})

    Returns:
        ndarray -- column transformed
        list of str -- names of the future columns
        Pipeline -- pipeline
    """

    if fit:
      pipeline_ohe[transformation].fit(data[column].values.reshape(-1, 1))

    ohe_cat = pipeline_ohe[transformation].transform(
        data[column].values.reshape(-1, 1)).toarray()

    cols = pipeline_ohe[transformation].get_feature_names()

    return ohe_cat, cols, pipeline_ohe

  def transform(self, data):
    """applies transformations to data
      Keyword Arguments:
        data {data frame} -- data to be transformed
    """

    if self.__mode == 'train':
      # creating the pipelines
      pipeline_ohe = pipeline.Pipeline(
          [('ohe_1', preprocessing.OneHotEncoder(handle_unknown='ignore')),
           ('ohe_2', preprocessing.OneHotEncoder(handle_unknown='ignore'))])

      # categorical feature 1
      ohe_cat1, columns_1, pipeline_ohe = self._categorical_to_ohe(
          data, pipeline_ohe, 'ohe_1', 'categorical_target_1')

      # categorical feature 2
      ohe_cat2, columns_2, pipeline_ohe = self._categorical_to_ohe(
          data, pipeline_ohe, 'ohe_2', 'categorical_target_2')

      utils.save_pipeline(pipeline_ohe,
                          self.config['location_pipeline_categorical'])

    if self.__mode == 'production':
      # loading pipelines
      pipeline_ohe = utils.load_pipeline(
          self.config['location_pipeline_categorical'])
      ohe_cat1, columns_1, _ = self._categorical_to_ohe(
          data, pipeline_ohe, 'ohe_1', 'categorical_target_1', fit=False)
      ohe_cat2, columns_2, _ = self._categorical_to_ohe(
          data, pipeline_ohe, 'ohe_2', 'categorical_target_2', fit=False)

    ohe_cat1_df = pd.DataFrame(ohe_cat1, columns=columns_1, dtype='float64')
    ohe_cat2_df = pd.DataFrame(ohe_cat2, columns=columns_2, dtype='float64')

    # concatenating back to original data frame
    data = data.join(ohe_cat1_df, how='left')
    data = data.join(ohe_cat2_df, how='left')
    data = data.fillna(0.0)
    data.drop(['categorical_target_1', 'categorical_target_2'],
              axis=1,
              inplace=True)

    data = self._decide_embeddings_type(data)

    return data

  def _decide_embeddings_type(self, data):
    """[summary]

    Arguments:
        data {[type]} -- [description]

    Returns:
        [type] -- [description]
    """
    if self.__embeddings is 'glove':
      text = data.features.to_numpy()
      padded_sentences = self.text_to_vector_embeddings(text)
      # adding to pandas with original data padded sequences
      data['padded_sentences'] = padded_sentences

    if self.__embeddings is 'elmo':
      # load pickles
      pass

    return data

  def split_data(self, data, train_size=0.8, validation_size=0.2):
    """splits data in parts

    Keyword Arguments:
        data {data frame} -- data to be splitted
        train_size {float} -- sample size used (default: {0.8})
        validation_size {float} -- sample size used (default: {0.2})

    Returns:
        data frame -- train part
        data frame -- validation part
    """
    data.drop(['id', 'features'], axis=1, inplace=True)
    train, validation = utils.split_data(
        data, train_size=train_size, validation_size=validation_size)

    return train, validation
