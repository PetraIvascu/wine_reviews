"""model.py contains class WineModel and WineModelTraining
"""
import os
import math
import numpy as np
import pandas as pd
import keras

from keras.layers import Dense, Input, Flatten, Dropout
from keras.layers.embeddings import Embedding
from keras.models import Model, load_model
from keras.callbacks import LearningRateScheduler
from keras import metrics
from keras.optimizers import Adam
from keras.utils import plot_model

import utils
from configs import configs


class WineModel:
  """ wine model
  """

  def __init__(self):
    self.config = configs['model']

  def save(self):
    """saves model on disk
    """
    self.model.save(self.config['location_model'])

  def load(self):
    """loads model location mentioned in config file
    """
    self.model = load_model(self.config['location_model'])

  def get_architecture(self, vocab_size, output_dim, max_lenght, weights,
                       numerical_features_size, dropout_eval):
    """creates the architecture the model will use to train with

    Arguments:
        vocab_size {int} -- length of vocabulary
        [see keras documentation: Embedding]
        output_dim {int} -- output dimension
        [see keras documentation: Embedding]
        max_lenght {int} -- length of input
        [see keras documentation: Embedding]
        weights {dict} -- dictionary containing corpus mapped to word vectors
        numerical_features_size {int} -- number of columns without text & labels
        dropout_eval {bool} -- option of using dropout at test time
    """

    if dropout_eval:
      value = dropout_eval
    else:
      value = None

    txt_input = Input(shape=(max_lenght,), dtype='int32', name='txt_input')
    x = Embedding(
        output_dim=output_dim,
        input_dim=vocab_size,
        input_length=max_lenght,
        weights=[weights],
        trainable=True)(txt_input)
    txt_out = Flatten()(x)
    txt_out = Dropout(0.2)(txt_out, training=value)
    txt_out = Dense(50, activation='relu', name='text_output')(txt_out)

    main_input = Input(shape=(numerical_features_size,), name='main_input')
    lyr = keras.layers.concatenate([main_input, txt_out])
    lyr = Dense(128, activation="relu")(lyr)
    lyr = Dropout(0.2)(lyr, training=value)
    main_output = Dense(1, activation='linear', name='main_output')(lyr)

    self.model = Model(inputs=[main_input, txt_input], outputs=[main_output])

  def complile(self):
    """compiles model
    """
    self.model.compile(
        loss=self.config['loss'],
        optimizer=Adam(lr=self.config['lr']),
        metrics=[metrics.mae])

  def save_architecture(self):
    """saves the acrhitecture of the model
    """
    plot_model(
        self.model,
        to_file=os.path.join(self.config['save_fig'], 'wine_model.png'))

  def _step_decay(self, epoch):
    """ learning rate scheduler

    Arguments:
        epoch {int} -- number of epochs to train for

    # Returns:
        float32-- new learning rate
    """
    initial_lrate = 0.001
    drop = 0.5
    epochs_drop = 10.0
    lrate = initial_lrate * math.pow(drop, math.floor(
        (1 + epoch) / epochs_drop))
    return lrate

  def fit(self, train, validation, batch_size, epochs, shuffle, verbosity):
    """trains a model

    Arguments:
        train {data frame} -- data points for training
        validation {data frame} -- data points for validation
        batch_size {int} -- batch to use when train
        epochs {int} -- number of epochs to train
        shuffle {bool} -- to shuffle or not the data
        verbosity {int} -- displaying information on console
    """
    lrate = LearningRateScheduler(self._step_decay)
    history = self.model.fit(
        [train.to_numpy()[:, 1:-2],
         np.array(train.padded_sentences.to_list())],
        train.continous_target_1.to_numpy(),
        batch_size=batch_size,
        epochs=epochs,
        shuffle=shuffle,
        verbose=verbosity,
        callbacks=[lrate],
        validation_data=([
            validation.to_numpy()[:, 1:-2],
            np.array(validation.padded_sentences.to_list())
        ], validation.continous_target_1.to_numpy()))

  def eval(self, data):
    """evaluates data requiers labels too
    Arguments:
        data {data frame} -- (x,y) pairs
    """
    x_eval = [
        data.to_numpy()[:, 1:-2],
        np.array(data.padded_sentences.to_list())
    ]
    y_eval = data.continous_target_1.to_numpy()

    metrics_eval = self.model.evaluate(x_eval, y_eval)

    return metrics_eval

  def evaluation_report(self, results, data, top_k):
    """computes a report based on results and given test set

    Arguments:
        results {data frame} -- results from prediction
        data {data frame} -- evaualted data
        top_k {int} -- top k elements to be returned

    Returns:
        [data frame] -- top k biggest averaged error per categorical_target_1
                                                     and categorical_target_2
    """

    joined = results.join(data, how='left', lsuffix='_left', rsuffix='_right')
    joined['abs_error'] = (joined['mean'] - joined['continous_target_1']).abs()
    groups_cat_1 = joined.groupby(['categorical_target_1'])
    groups_cat_2 = joined.groupby(['categorical_target_2'])

    stat_cat_1 = groups_cat_1.abs_error.mean().to_frame()
    stat_cat_1['continous_target_1'] = stat_cat_1.index.to_numpy()
    stat_cat_1 = stat_cat_1.reset_index(drop=True)
    n_largest_cat_1 = stat_cat_1.nlargest(top_k, 'abs_error')

    stat_cat_2 = groups_cat_2.abs_error.mean().to_frame()
    stat_cat_2['continous_target_2'] = stat_cat_2.index.to_numpy()
    stat_cat_2 = stat_cat_2.reset_index(drop=True)
    n_largest_cat_2 = stat_cat_2.nlargest(top_k, 'abs_error')

    return n_largest_cat_1, n_largest_cat_2

  def predict_cetainty(self, ids, data, times_repeat_eval):
    """predicts the uncertainty of model through usage of dropout at test time

    Arguments:
        ids {ndarray} -- requiered for later identification
        data {data frame} -- data to predict label for
        times_repeat_eval {int} -- number of times each instance will be evaluated

    Returns:
        list of ndarray -- [ids, mean, std]
    """
    x_eval = [
        data.to_numpy()[:, 1:-2],
        np.array(data.padded_sentences.to_list())
    ]

    certainty = np.array([])
    mean = np.array([])

    for step, _ in enumerate(x_eval[0]):
      predictions = np.array([])
      for _ in range(0, times_repeat_eval):
        pred = self.model.predict([[x_eval[0][step]], [x_eval[1][step]]])
        predictions = np.append(predictions, pred[0])

      certainty = np.append(certainty, np.std(predictions))
      mean = np.append(mean, np.mean(predictions))

    return [ids, mean, certainty]

  def result_uncertainty_to_data_frame(self, results, location):
    """prints results

    Arguments:
        results {list of ndarray} -- contains ids, means and stds
                                      from prediction
    """

    results_dict = {'id': results[0], 'mean': results[1], 'std': results[2]}
    results_df = pd.DataFrame(results_dict, columns=results_dict.keys())
    # saving into csv file
    results_df.to_csv(location, index=False)
    return results_df
