#!/usr/bin/env python

from entities.wine_evaluate import WineModelEval


def main():
  wine_eval = WineModelEval()
  # reads data from location defined in eval.py config file
  wine_eval.read_data()

  # processes data: inputs for missing values, converts data, scales data
  wine_eval.transform_data()
  # load model
  wine_eval.load_model()
  # evaluates model uses given the labels too
  metrics = wine_eval.evaluate_model()
  print('loss validation: %.2f mae validation: %.2f' % (metrics[0], metrics[1]))
  # predicts and saves results to csv (location: ./model/)
  results = wine_eval.predict_uncertainty(top_k=5)
  # display results
  wine_eval.display_report(results[0])
  wine_eval.display_report(results[1])


if __name__ == "__main__":
  main()