# Dataset
Sample taken from [wine reviews](https://www.kaggle.com/zynicide/wine-reviews) Kaggle.

Columns used: description, country, type_wine, points, price. Those were renaimed in: features,categorical_target_1, categorical_target_2, continuous_target_1,continous_target_2.

# Task

The approach to the given task was to frame the problem as a regression. This will allow to use all the information given.
Based on data analisys the column named continous_target_1 had no missing values
as opposed to categorigal_target_1/2 or continous_target_2.
The model is trained on all given information by using embedding layers for text
and dense ones for the numerical information. The model's architecture is saved
in ./figures/wine_model.png.

![The architecture used](./figures/wine_model.png)


Mention: summary of the data can also be found in notebook data_analysis.ipynb
along with other plots. Given the entire structure of the project I considered
to be better if the summary will be made by a script (summary.py)

Uncertainty: to express it the best way is to use Bayesian Neural Networks. Due to the fact that they are notorious slow to train, especially for a
real life contex, other solution emerge as a viable option to express this metric.
One of them is to use dropout at test time. This implies that each instance from
test set will be fead to the predict method multiple times. It turns out that the
mean taken over all the predictions aproximate well the final prediction and the
standard deviation computed in the same manner measures well the uncertainty.
A few resources:
[resource 1](https://towardsdatascience.com/uncertainty-estimation-for-neural-network-dropout-as-bayesian-approximation-7d30fc7bc1f2),
[resource 2](https://amethix.com/know-what-you-predict-estimating-uncertainty-with-neural-networks/)

At the prediction time we noticed that indivuduals less well represented within dataset have also a higher uncertainty score. E.g.: Bulgaria has aprox. 100 entries, Mexico aprox 50 vs USA with 40000 (in training_data.csv).


### Prerequisites

Project uses Keras, glove embeddings, which are included in the folder embeddings.
It should exist any other prerequisites.


### Installing


Installing the dependencies with command:

```
$pip install -r requirements.txt
```

### Run
To see a summary for data run summary.py with command:
```
python summary.py ./path/to/data/data.csv
```
Where ./path/to/data/data.csv --> ./data/raw/train/train_data.csv, for instance
The summary will be saved in './data/summary.csv'. Summary has NaN where operations
were not possible to be computed e.g: min on categorical_target_1.

To run an evaluation round please provide a similar file to train_data.csv and
place it into './data/raw/test'. But if location is not convinient please change
the path from './configs/eval.py' by giving "location_data" another value.

The above aspect can be applied to any parameters found in directory configs.

Then run the command below:
```
python evaluate.py
```
The script will process data, load model, evaluate and predict uncertainty.

Method eval_model() will use the groud truths to evaluate, while
predict_uncertainty() predicts labels and uncertainty. Those results are saved
to data frame which can be found './model/results_prediction.csv'. If you wish
to see them side by side with the ground truths use this piece of code:

```
results = pd.read_csv('./model/results_prediction.csv')
test_data = pd.read_csv('./data/test/test_data.csv')
results_new = results.join(test_data.continuous_target_1, how='left')

```








